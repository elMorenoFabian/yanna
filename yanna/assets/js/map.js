let dragged = null;
let count = 0;

const source = document.querySelectorAll(".draggable");

source.forEach(element => {
    element.addEventListener("dragstart", (event) => {
        dragged = event.target;
    });
});

const target = document.querySelectorAll(".droptarget");
target.forEach(element => {
    element.addEventListener("dragover", (event) => {
        event.preventDefault();
    });

    element.addEventListener("drop", (event) => {
        event.preventDefault();
        element.appendChild(dragged);
        if (dragged.dataset.drag === event.target.dataset.drop) {
            count ++;
        }

        if (count == 3) {
            document.querySelector('.js-modal').classList.add('open');
            source.forEach(element => {
                element.draggable = false;
                element.classList.remove('draggable')
            });
            target.forEach(element => {
                element.style.border = 0;
            });
        }
    });
});

document.querySelector('.js-btn-close').addEventListener('click', () => {
    document.querySelector('.js-modal').classList.remove('open');
});
